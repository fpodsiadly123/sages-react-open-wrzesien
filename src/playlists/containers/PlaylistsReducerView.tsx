import { useEffect } from "react";
import { Playlist } from "../../core/model/Playlist";
import { FormikPlaylistForm } from "../components/FormikPlaylistForm";
import { PlaylistDetails } from "../components/PlaylistDetails";
import { PlaylistForm } from "../components/PlaylistForm";
import { PlaylistsList } from "../components/PlaylistsList";
import {
  selectedPlaylistSelector,
  actions,
  playlistsSelector,
  playlistFeatureSelector,
  fetchPlaylists,
} from "../../core/reducers/PlaylistsReducer";
import { useDispatch, useSelector } from "react-redux";

interface Props {}

export const PlaylistsReducerView = (props: Props) => {
  const dispatch = useDispatch();
  const { selectedId, mode } = useSelector(playlistFeatureSelector);
  const items = useSelector(playlistsSelector);
  const selectedPlaylist = useSelector(selectedPlaylistSelector);

  useEffect(() => {
    // if (items.length == 0) dispatch(actions.load(playlistsMock));
    // fetchPlaylists(dispatch)
    dispatch(fetchPlaylists())
  }, []);

  const removePlaylist = (id: Playlist["id"]) => dispatch(actions.remove(id));

  const selectPlaylistById = (id: Playlist["id"]) =>
    dispatch(actions.select(id));

  const savePlaylist = (draft: Playlist) => dispatch(actions.update(draft));

  const createPlaylist = (draft: Playlist) => {
    draft.id = (Date.now() + Math.ceil(Math.random() * 100_000)).toString();
    dispatch(actions.create(draft));
  };
  const cancel = () => dispatch(actions.changemode("details"));

  return (
    <div>
      {/* .row>.col*2 */}
      <div className="row">
        <div className="col">
          <PlaylistsList
            playlists={items}
            selectedId={selectedId}
            onRemove={removePlaylist}
            onSelect={selectPlaylistById}
          />
          <button
            className="btn btn-info float-end mt-3"
            onClick={() => dispatch(actions.changemode("create"))}
          >
            Create new playlist
          </button>
        </div>
        <div className="col">
          {mode === "details" ? (
            <div>
              <PlaylistDetails
                playlist={selectedPlaylist}
                onEdit={() => dispatch(actions.changemode("edit"))}
              />
            </div>
          ) : null}

          {selectedPlaylist && mode === "edit" && (
            <div>
              <FormikPlaylistForm
                playlist={selectedPlaylist}
                onCancel={cancel}
                onSave={savePlaylist}
              />
            </div>
          )}

          {selectedPlaylist && mode === "create" && (
            <div>
              <PlaylistForm
                playlist={{
                  id: "",
                  description: "",
                  name: "",
                  public: false,
                }}
                onCancel={cancel}
                onSave={createPlaylist}
              />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
