import React, {
  useEffect,
  useRef,
  useState,
} from "react";
import { Playlist } from "../../core/model/Playlist";

interface Props {
  playlist: Playlist;
  onCancel: () => void;
  onSave: (draft: Playlist) => void;
}

export const PlaylistForm = ({ playlist, onCancel, onSave }: Props) => {
  const [playlistName, setPlaylistName] = useState("");
  const [playlistPublic, setPlaylistPublic] = useState(false);
  const [playlistDescription, setPlaylistDescription] = useState("");

  useEffect(() => {
    setPlaylistName(playlist.name);
    setPlaylistPublic(playlist.public);
    setPlaylistDescription(playlist.description);
  }, [playlist]);

  const inputRef = useRef<HTMLInputElement | null>(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  const submitForm = (
    // event: React.MouseEvent<HTMLInputElement, MouseEvent>
    event: React.FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault(); // Block form submission

    onSave({
      ...playlist,
      name: playlistName,
      public: playlistPublic,
      description: playlistDescription,
    });
  };

  return (
    <form onSubmit={submitForm}>
      <div className="form-group mb-3">
        <label htmlFor="">Name:</label>
        <input
          id="playlistNameInput"
          // ref={elem => setSterREf(elem)}
          ref={inputRef}
          type="text"
          className="form-control"
          value={playlistName}
          onChange={(event) => setPlaylistName(event.currentTarget.value)}
        />
        <span>{playlistName.length} / 170</span>
      </div>

      <div className="form-group mb-3">
        <label>
          <input
            type="checkbox"
            defaultChecked={playlistPublic}
            onChange={(e) => setPlaylistPublic(e.currentTarget.checked)}
          />
          Public
        </label>
      </div>

      <div className="form-group mb-3">
        <label htmlFor="">Description:</label>
        <textarea
          className="form-control"
          onChange={(e) => setPlaylistDescription(e.currentTarget.value)}
          value={playlistDescription}
        />
      </div>

      <input
        type="button"
        value="Cancel"
        className="btn btn-danger"
        onClick={onCancel}
      />
      <input type="submit" value="Save" className="btn btn-danger" />
    </form>
  );
};
