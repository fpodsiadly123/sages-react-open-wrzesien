import axios from "axios";
import { string } from "prop-types";
import React, { useEffect, useState } from "react";
import { useOAuth2Token, OAuthCallback } from "react-oauth2-hook";
import { User } from "../model/User";
import { fetchCurrentUserProfile } from "../services/user";

class NoProviderError extends Error {
  constructor() {
    super("No provider for User Context");
  }
}

interface Ctx {
  user: User | null;
  token?: string;
  login(): void;
  logout(): void;
}

const initialContext: Ctx = {
  user: null,
  login(): void {
    throw new NoProviderError();
  },
  logout(): void {
    throw new NoProviderError();
  },
};

export const UserProfileContext = React.createContext(initialContext);

interface Props {
  // children: React.ReactNode
}

export const UserProfileContextProvider: React.FC<Props> = ({ children }) => {
  const [user, setUser] = useState<User | null>(null);

  const [token, refreshToken, setToken] = useOAuth2Token({
    authorizeUrl: "https://accounts.spotify.com/authorize",
    clientID: "58a8082f61dd4af7940b8f1205bf9665",
    scope: [],
    redirectUri: document.location.origin + "/callback",
  });

  const updateInterceptor = (token?: string, InterceptorHandle?: number) => {
    InterceptorHandle && axios.interceptors.request.eject(InterceptorHandle);
    if (!token) {
      return;
    }
    return axios.interceptors.request.use((config) => {
      config.headers["Authorization"] = `Bearer ${token}`;
      return config;
    });
  };

  const [InterceptorHandle, setInterceptorHandle] = useState(() =>
    updateInterceptor(token, 1)
  );

  useEffect(() => {
    if (!token) {
      return;
    }
    setInterceptorHandle(updateInterceptor(token, InterceptorHandle));

    fetchCurrentUserProfile().then((user) => {
      setUser(user);
    });
  }, [token]);

  if (window.location.pathname === "/callback") {
    return <OAuthCallback />;
  }
  const value = {
    user,
    token,
    login() {
      refreshToken();
    },
    logout() {
      setUser(null);
      setToken(undefined);
    },
  };

  return (
    <UserProfileContext.Provider value={value}>
      {children}
    </UserProfileContext.Provider>
  );
};
