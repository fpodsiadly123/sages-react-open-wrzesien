import axios from "axios";
import { Album, AlbumsSearchResponse } from "../model/Search";


export const fetchSearchResults = (query: string, type = 'album') => {

    return axios.get<AlbumsSearchResponse>('https://api.spotify.com/v1/search', {
        // headers: {
        //     Authorization: 'Bearer tajnytokenplacki'
        // },
        params: {
            q: query,
            type: type
        }
    })
        .then(res => {
            return res.data.albums.items
        })
        .catch(err => {
            return Promise.reject(new Error(err.response.data.error.message))
        })
}

export const fetchAlbumById = (id: Album['id']) => {
    return axios.get<Album>(`https://api.spotify.com/v1/albums/${id}`)
        .then(res => {
            return res.data
        })
        .catch(err => {
            return Promise.reject(new Error(err.response.data.error.message))
        })
}

// export const fetchSearchResults = (query: string, type = 'album') => {

//     return axios.get('/albums.json')
//         .then(res => {
//             return res.data
//         })

// }