import React from "react";
import { Track } from "../../core/model/Search";

interface Props {
  tracks: Track[];
  onPlay(track: Track): void;
}

export const TrackList = ({ tracks, onPlay }: Props) => {
  return (
    <div className="list-group">
      {tracks.map((track) => (
        <div
          className="list-group-item"
          key={track.id}
          onClick={() => onPlay(track)}
        >
          {track.track_number}. {track.name}
        </div>
      ))}
    </div>
  );
};
