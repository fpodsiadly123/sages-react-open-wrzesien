```js
[1,2,3,4,5].reduce( ( sum, x ) => {
    console.log(sum, x )
    return sum + x
},0)
react_devtools_backend.js:4049 0 1
react_devtools_backend.js:4049 1 2
react_devtools_backend.js:4049 3 3
react_devtools_backend.js:4049 6 4
react_devtools_backend.js:4049 10 5
15

```

```js

[1,2,3,4,5].reduce( ( state, x ) => {
    console.log(state, x )
    return {
        ...state,
        counter: state.counter + x
    }
},{
    counter: 0, todos:[]
})
react_devtools_backend.js:4049 {counter: 0, todos: Array(0)} 1
react_devtools_backend.js:4049 {counter: 1, todos: Array(0)} 2
react_devtools_backend.js:4049 {counter: 3, todos: Array(0)} 3
react_devtools_backend.js:4049 {counter: 6, todos: Array(0)} 4
react_devtools_backend.js:4049 {counter: 10, todos: Array(0)} 5
{counter: 15, todos: Array(0)}

```

```js
inc = x => x + 1;
dec = x => x - 1;

[inc,inc,dec,inc].reduce( ( state, fn ) => {

    return { ...state, counter: fn(state.counter) }
},{
    counter: 0, todos:[]
})
{counter: 2, todos: Array(0)}
```

```js

inc = (payload=1) => x => x + payload;
dec = (payload=1) => x => x - payload;


[inc(2),inc(),dec(2),inc()].reduce( ( state, fn ) => {

    return { ...state, counter: fn(state.counter) }
},{
    counter: 0, todos:[]
})
{counter: 2, todos: Array(0)}

```

```js

inc = (payload=1) => ({type:'INC', payload} );
dec = (payload=1) => ({type:'DEC', payload} );
addTodo = (payload='todo') => ({type:'ADD_TODO', payload} );

[inc(2),inc(),addTodo('kup placki!'), dec(3),inc(2)].reduce( ( state, action ) => {
    switch(action.type){
        case 'INC': return { ...state, counter: state.counter + action.payload }
        case 'DEC': return { ...state, counter: state.counter - action.payload }
        case 'ADD_TODO': return { ...state, todos: [...state.todos, action.payload ] }
        default: return state;
    }
},{
    counter: 0, todos:[]
})
{counter: 2, todos: Array(1)}
counter: 2
todos: ['kup placki!']


```

```js
inc = (payload = 1) => ({ type: "INC", payload });
dec = (payload = 1) => ({ type: "DEC", payload });
addTodo = (payload = "todo") => ({ type: "ADD_TODO", payload });

reducer = (state, action) => {
  switch (action.type) {
    case "INC":
      return { ...state, counter: state.counter + action.payload };
    case "DEC":
      return { ...state, counter: state.counter - action.payload };
    case "ADD_TODO":
      return { ...state, todos: [...state.todos, action.payload] };
    default:
      return state;
  }
};

console.log((state = { counter: 0, todos: [] }));
//  {counter: 0, todos: Array(0)}
console.log((state = reducer(state, inc(2))));
//  {counter: 2, todos: Array(0)}
console.log((state = reducer(state, inc())));
//  {counter: 3, todos: Array(0)}
console.log((state = reducer(state, addTodo("kup placki!"))));
//  {counter: 3, todos: Array(1)}
console.log((state = reducer(state, dec(3))));
//  {counter: 0, todos: Array(1)}
console.log((state = reducer(state, inc(3))));
//  {counter: 3, todos: Array(1)}
```

```js

inc = (payload=1) => ({type:'INC', payload} );
dec = (payload=1) => ({type:'DEC', payload} );
addTodo = (payload='todo') => ({type:'ADD_TODO', payload} );


counter = ( state = 0, action ) => {
    switch(action.type){
        case 'INC': return state + action.payload }
        case 'DEC': return state - action.payload }
    }
};

reducer =  ( state, action ) => {
    switch(action.type){
        case 'ADD_TODO': return { ...state, todos: [...state.todos, action.payload ] }
        default: return {
            ...state,
            counter: counter(state.counter,action),
            // todos: todos(state.todos,action),
            // playlists: playlists(state.playlists,action),
            // search: search(state.search,action),
        };
    }
};

console.log(state = {counter: 0, todos:[]});
//  {counter: 0, todos: Array(0)}
console.log(state = reducer(state, inc(2)));
//  {counter: 2, todos: Array(0)}
console.log(state = reducer(state, inc()));
//  {counter: 3, todos: Array(0)}
console.log(state = reducer(state, addTodo('kup placki!')));
//  {counter: 3, todos: Array(1)}
console.log(state = reducer(state, dec(3)));
//  {counter: 0, todos: Array(1)}
console.log(state = reducer(state, inc(3)));
//  {counter: 3, todos: Array(1)}


```
