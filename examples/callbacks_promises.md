```js 

function echo(msg, successCb){
    setTimeout(()=>{
        successCb(msg)
    },1000)
}

echo('Ala ma kota', res => console.log(res) )
undefined
react_devtools_backend.js:4049 Ala ma kota
echo('ala', res => {
    echo( res + ' ma ', res=> {   
        echo( res + ' kota ', res=> {
            console.log(res)
        })
    })
})
undefined
react_devtools_backend.js:4049 ala ma  kota 

```

```js

function echo(msg){
    return new Promise((resolve)=>{
        
        setTimeout(()=>{
            resolve(msg)
        },1000)

    })
}

promise = echo('Ala ma kota' )

// ===

promise.then(  res => console.log(res) )
// Promise {<pending>}
// Ala ma kota

// promise.then(  res => console.log(res) )
// Ala ma kota

// Promise {<fulfilled>: undefined}


promise.then(  res => console.log(res) )
promise.then(  res => console.log(res) )
promise.then(  res => console.log(res) )

setTimeout(()=>{
    promise.then(  res => console.log(res) )
},2000)

// Ala ma kota
// Ala ma kota
// Ala ma kota
// Ala ma kota

```

```js

function echo(msg){
    return new Promise((resolve)=>{
        
        setTimeout(()=>{
            resolve(msg)
        },1000)

    })
}

promise = echo('Ala' )

p2 = promise.then( res => res + ' ma ')
// p3 = p2.then( res => res + 'kota ')
p3 = p2.then( res => {
    return echo(res + 'kota ')
})
p3.then(console.log)

// promise.then( res => {
//      echo(res + ' ma' ).then(res => {
//          echo(res + ' kota' ).then(res => {
//             console.log(res)
//          })
//      })
// })

// Promise {<pending>}
//  Ala ma kota 

```

```js

function echo(msg, err){
    return new Promise((resolve, reject)=>{
        
        setTimeout(()=>{
            err? reject(err) : resolve(msg)
        },1000)

    })
}

promise = echo('Ala', 'upss' )

p2 = promise.then( res => res + ' ma ')
// p3 = p2.then( res => res + 'kota ')
p3 = p2.then( res => {
    return echo(res + 'kota ')
})
p3.then(console.log)

// Promise {<pending>}
//  Uncaught (in promise) upss

```

## Error handling

```js

function echo(msg, err){
    return new Promise((resolve, reject)=>{
        
        setTimeout(()=>{
            err? reject(err) : resolve(msg)
        },1000)

    })
}

promise = echo('Ala', 'upss' )

promise
.then( res => res + ' ma ',  err => 'Wszyscy mamy ')
.then( res => echo(res + 'kota '))
.then(console.log)

// Promise {<pending>}
//  Wszyscy mamy kota 

```

```js

function echo(msg, err){
    return new Promise((resolve, reject)=>{
        
        setTimeout(()=>{
            err? reject(err) : resolve(msg)
        },1000)

    })
}

promise = echo('Ala', 'upss' )
// .then ( handleResolve, handleReject )
promise
// .then( res => res + ' ma ',  err => 'Wszyscy mamy ')
.then( res => res + ' ma ',  err => Promise.reject('Nie ma nic') )
.then( res => echo(res + 'kota ', 'nie ma kota'))
.catch( err => 'Wystapil blad ' + err)
.then(console.log)

// Promise {<pending>}
// Wystapil blad Nie ma nic

```

```js

fetch('http://localhost:3000/albums.json')
.then( resp => resp.json() )
.then(console.log)
// Promise {<pending>}

// (4) [{…}, {…}, {…}, {…}]
// 0: {id: '123', name: 'Album 123', images: Array(1)}
// 1: {id: '234', name: 'Album 234', images: Array(1)}
// 2: {id: '345', name: 'Album 345', images: Array(1)}
// 3: {id: '456', name: 'Album 456', images: Array(1)}

```